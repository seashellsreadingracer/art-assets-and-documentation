There are so many different things you can be when you grow up. 
<sb>For example, you could be a doctor, a teacher, 
<sb>a baseball player, a construction worker, a writer, 
<sb>a scientist, or many other things. 
<sb>People choose different jobs for different reasons. 
<sb>Some people like to have children when they grow up, 
<sb>and some people don’t. 
<sb>Most people get a car or something else to ride around in. 
<sb>You could ride a bicycle. 
<sb>You could ride a horse. 
<sb>You could drive a truck, or maybe even fly a helicopter! 
<sb>We call these different things vehicles. 
<sb>People also choose many different places to live. 
<sb>You could live in a big city or a small town. 
<sb>You might like to live near the mountains or a lake. 
<sb>Or maybe you would like to live in a different country. 
<sb>You could also live in different types of buildings. 
<sb>Some people live in a house. 
<sb>Some live in an apartment. 
<sb>Some people even spend a whole year camping outdoors.
