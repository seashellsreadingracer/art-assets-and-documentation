Storms in the spring can bring lightning. 
<sb>Lightning moves through the air. 
<sb>It sometimes hits the ground. 
<sb>Lightning can hurt people. 
<sb>It can harm trees and buildings. 
<sb>Stay away from lightning. 
<sb>If a lightning storm comes, 
<sb>there are four things you can do to stay safe. 
<sb>Go indoors. 
<sb>Stay away from water. 
<sb>Do not use the telephone. 
<sb>Turn off the television or computer.