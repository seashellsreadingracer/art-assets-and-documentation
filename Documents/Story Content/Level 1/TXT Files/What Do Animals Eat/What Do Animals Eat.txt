Many kinds of animals live in zoos. 
<sb>In some zoos, cooks fix food for the animals. 
<sb>Zebras eat hay and giraffes eat carrots. 
<sb>Monkeys like fruit. 
<sb>Cooks say that elephants like a lot of things, 
<sb>even banana peels! 
<sb>Wild animals find their own food. 
<sb>Bears eat meat, fish and plants. 
<sb>Lions eat meat like wild rabbits. 
<sb>Some fish eat other fish. 
<sb>Other fish eat plants. 
<sb>Prairie dogs eat plants. 
<sb>And snakes eat meat, such as mice. 





