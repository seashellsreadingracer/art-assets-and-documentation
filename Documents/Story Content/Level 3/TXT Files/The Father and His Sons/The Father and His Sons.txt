A father had a family of sons who were always arguing. 
<sb>He couldn’t get them to stop fighting by talking to them. 
<sb>So, he determined to show them what happens if people don’t stick together. 
<sb>SO he told them to bring him a bundle of sticks. 
<sb>When they had done so, he placed the bundle into the hands of each of them one at a time, 
<sb>and told them to break it in pieces. 
<sb>They tried with all their strength, and were not able to do it. 
<sb>He next opened the bundle, took the sticks separately, one by one, 
<sb>and again put them into his sons’ hands, upon which they broke them easily. 
<sb>He then told them, “My sons, if you are of one mind, and unite to assist each other, 
<sb>you will be as this bundle, not hurt by all the attempts of your enemies. <sb>But, if you are divided among yourselves, you will be broken as easily as these sticks.”