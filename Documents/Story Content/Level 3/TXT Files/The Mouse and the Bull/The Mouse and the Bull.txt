A bull was bitten by a Mouse and, angered by the wound, tried to capture him. <sb>But the Mouse reached his hole in safety. 
<sb>Though the Bull dug into the walls with his horns, 
<sb>he tired before he could rout out the Mouse, and crouching down, 
<sb>went to sleep outside the hole. 
<sb>The Mouse peeped out, crept furtively up his flank, 
<sb>and again biting him, retreated to his hole. 
<sb>The Bull, rising up, and not knowing what to do, was sadly perplexed. 
<sb>At which the Mouse said, “The great do not always prevail. 
<sb>There are times when the small and the lowly are the strongest to do mischief.”