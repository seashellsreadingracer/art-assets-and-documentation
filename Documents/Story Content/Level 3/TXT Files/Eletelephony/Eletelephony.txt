Once there was an elephant, 
<sb>Who tried to use the telephant-
<sb>No! 
<sb>No!
<sb>I mean an elephone
<sb>Who tried to use the telephone-
<sb>(Dear me!
<sb>I am not certain quite
<sb>That even now I’ve got it right.)
<sb>Howe’er it was, he got his trunk
<sb>Entangled in the telephonic;
<sb>The more he tried to get it free,
<sb>The louder buzzed the telephee-
<sb>(I fear I’d better drop the song
<sb>Of elephop and telephong!)
