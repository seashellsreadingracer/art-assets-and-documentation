There was once a farmer who had a fine olive orchard. 
<sb>He was very industrious, and the farm always prospered under his care. 
<sb>But he knew that his three sons despised the farm work, 
<sb>and were eager to make wealth fast, through adventure. 
<sb>When the farmer was old, and felt that his time had come to die, 
<sb>he called the three sons to him and said, 
<sb>"My sons, there is a pot of gold hidden in the olive orchard. 
<sb>Dig for it, if you wish it." 
<sb>The sons tried to get him to tell them in what part of the orchard 
<sb>the gold was hidden; but he would tell them nothing more. 
<sb>After the farmer was dead, the sons went to work to find the pot of gold; 
<sb>since they did not know where the hiding-place was, 
<sb>they agreed to begin in a line, at one end of the orchard, 
<sb>and to dig until one of them should find the money. 
<sb>They dug until they had turned up the soil from one end of the orchard to the other, 
<sb>round the tree-roots and between them. 
<sb>But no pot of gold was to be found. 
<sb>It seemed as if some one must have stolen it, 
<sb>or as if the farmer had been wandering in his wits. 
<sb>The three sons were bitterly disappointed to have all their work for nothing. 
<sb>The next olive season, 
<sb>the olive trees in the orchard bore more fruit than they had ever given; 
<sb>the fine cultivating they had had from the digging brought so much fruit, 
<sb>and of so fine a quality, 
<sb>that when it was sold it gave the sons a whole pot of gold! 
<sb>And when they saw how much money had come from the orchard, 
<sb>they suddenly understood what the wise father had meant when he said, 
<sb>"There is gold hidden in the orchard; dig for it."