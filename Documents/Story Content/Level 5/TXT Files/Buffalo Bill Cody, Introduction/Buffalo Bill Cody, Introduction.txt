William Frederick Cody, known as Buffalo Bill, 
<sb>was a buffalo hunter, United states army scout, and an Indian fighter. 
<sb>But he is probably best known as the man who gave the Wild West its name. 
<sb>He was born in 1846 and died in 1917. 
<sb>He produced a colorful show (not a television show) 
<sb>called “Buffalo Bill’s Will West and Congress of Rough Riders of the World,” 
<sb>which had an international reputation 
<sb>and helped create a lasting image of the American West.