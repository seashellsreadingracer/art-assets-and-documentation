Once upon a time Silly Pilly was going to go to school for the first time. 
<sb>He asked his mother, "Mommy, what if I get hungry?" 
<sb>"That's okay," said his mother. "I'll give you a lunch to bring." 
<sb>"What if I get thirsty?" said Silly Pilly. 
<sb>"That's okay," said his mother. "I'll give you some milk to bring." 
<sb>"But how will I keep the milk cold?" said Silly Pilly. 
<sb>"That's okay," said his mother. "You can bring along a refrigerator." 
<sb>(Silly Pilly's mother was a little silly, too!) 
<sb>"But what if I have to go to the bathroom?" said Silly Pilly. 
<sb>"That's okay," said his mother. "You can bring along a toilet." 
<sb>"But how will I wash my hands afterwards?" said Silly Pilly. 
<sb>"That's okay," said his mother. "You can bring along a sink." 
<sb>"But what if I want to take a nap?" said Silly Pilly. 
<sb>"That's okay," said his mother. "You can bring along your bed." 
<sb>The next morning, Silly Pilly was ready to go to school. 
<sb>He had so much stuff to take along that they had to get a truck to put it in! 
<sb>It took six great big strong guys to load it all into the truck. 
<sb>The truck was so full of stuff that there was no room left for Silly Pilly! 
<sb>He had to go with his mother instead. 
<sb>When they got to school, Silly Pilly met his new teacher. 
<sb>"What's in the truck?" said the teacher. 
<sb>"Oh, that's just the stuff I need for school," said Silly Pilly. 
<sb>And he told the six great big strong guys to start unloading the truck. 
<sb>"Hold on!" said the teacher. "What's that?" 
<sb>"That's my bed," said Silly Pilly. 
<sb>"A bed!" said his teacher. "You don't need to bring your bed to school!" 
<sb>"But what if I want to take a nap?" said Silly Pilly. 
<sb>"We have mats to lie down on at nap time," said the teacher. 
<sb>"Oh," said Silly Pilly. 
<sb>And he told the six great big strong guys to put the bed back on the truck. 
<sb>"Wait!" said the teacher. "What's that?" 
<sb>"That's a sink," said Silly Pilly. 
<sb>"A sink!" said his teacher. "You don't need to bring a sink to school!" 
<sb>"But what if I need to wash my hands?" said Silly Pilly. 
<sb>"We already have sinks at this school," said the teacher. 
<sb>"Oh," said Silly Pilly. 
<sb>And he told the six great big strong guys to put the sink back on the truck. 
<sb>"Hey!" said the teacher. "What's that?" 
<sb>"That's a toilet," said Silly Pilly. 
<sb>"A toilet!" said his teacher. "Why did you bring a toilet to school?" 
<sb>"In case I have to go to the bathroom," said Silly Pilly. 
<sb>"We already have bathrooms at this school," said the teacher. 
<sb>"Oh," said Silly Pilly. 
<sb>And he told the six great big strong guys to put the toilet back on the truck. 
<sb>"Oh, my!" said the teacher. "What's that?" 
<sb>"That's a refrigerator," said Silly Pilly. 
<sb>"A refrigerator!" said his teacher. "Why did you bring a refrigerator to school?" 
<sb>"To keep my milk cold," said Silly Pilly. 
<sb>"We already have a refrigerator at this school," said the teacher. 
<sb>"Cool!" said Silly Pilly. 
<sb>And he told the six great big strong guys to take the refrigerator and go home. 
<sb>"Now what?" said the teacher. 
<sb>"I forgot to bring my milk," said Silly Pilly. 
<sb>"That's okay," said his teacher. "We have milk for you here." 
<sb>"Good," said Silly Pilly, "but I forgot my lunch too." 
<sb>"That's okay," said his teacher. "We have lunch here too." 
<sb>"Oh," said Silly Pilly. And the two of them walked into school. 
<sb>The End. 
